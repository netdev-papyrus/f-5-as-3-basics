<div align="center">
![as3_robot](img/as3_robot.png)
</div>

# AS3demo

Show AS3 declarations and tools of Gitlab to version them

[Download](https://github.com/F5Networks/f5-appsvcs-extension/releases) AS3 rpm

Users Guide [link](https://clouddocs.f5.com/products/extensions/f5-appsvcs-extension/latest/userguide/)

AS3 Best Practices [link](https://clouddocs.f5.com/products/extensions/f5-appsvcs-extension/latest/userguide/best-practices.html)

Composing a declaration [link](https://clouddocs.f5.com/products/extensions/f5-appsvcs-extension/latest/userguide/composing-a-declaration.html)

## Files Included {JSON Directory}

### generic.json

This declaration is usinge the Generic method, deploys a single application, a pool, and has two pools members on different ports. Also creates iRule *insert_log* and assigns to virtual server. iRule is hosted on Gitlab

**NOTE** Basic Auth is used for demo only, token auth is the preferred method for production [K69245523](https://support.f5.com/csp/article/K69245523)

- Basic Auth 
- Method: POST

### multi_app.json

This declaration is using the Service_HTTP and Generic method, deploys 2 applications with  two pools, and members. Notice how the iRule is **not** part of the declaration, and will be removed once deployed.
- Basic Auth
- Method: POST

### patch_newService.json

This declaration is using the op "operations" **add** and Service_HTTP to deploy a single application, pool, and two pool members to the existing Tenant from the above declarations.
- Basic Auth
- Method: PATCH

### add.json

This declaration also uses the operation **add**

- Basic Auth
- Method PATCH

```
"members": [{
                  "servicePort": 80,
                  "serverAddresses": [
                     "10.1.20.10"
                  ]},
                  {"servicePort": 81,
                  "serverAddresses": [
                     "10.1.20.11"
                  ]
               }]

```

When adding pool members (PATCH method) take note of the *path* agrument, the 1 below references the orignal placment from the example above. Array  positions start with 0 then 1, 2.. and so on. So position 0 is port *80 members* and position 1 is port *81 members*
This example below would add the pool member *10.1.20.12* to the port *81* members 
```
[{
	"op": "add",
	"path": "/Ten_01/App2/web1_pool/members/1/serverAddresses/-",
	"value": "10.1.20.12"
      }]
```

### delete_tenant.json

Using this file with the Ansible_Json ansible play **delete_http.yml** will remove the tenanat and verify
## Declarative AS3 Persistence

If you do not specify a persistence method, or none, Cookie persistence is the default. If we modified our generic.json file to have no persistence for our Web_vs app:
```
"App2": {
            "class": "Application",
            "Web_vs": {
               "class": "Service_HTTP",
               "virtualAddresses": [
                  "10.1.10.111"
               ],
               "pool": "web1_pool",
               "persistenceMethod": []
```

## Pool Members with different ports

Exert from *generic.json* file, this shows how to add pool members on unique ports:

```
"members": [{
                  "servicePort": 80,
                  "serverAddresses": [
                     "10.1.20.10"
                  ]},
                  {"servicePort": 81,
                  "serverAddresses": [
                     "10.1.20.11"
                  ]
               }]

```

**NOTE** When adding members with different ports, you will need to reference your orignal declaration. Service ports are set by python list placement. Element *0* represents all the port 80 members, element *1* represents all the port 81 members.

# Postman

Download [here](https://www.postman.com/downloads/)


# VS Code

Download [here](https://code.visualstudio.com/download)

F5 Extension for VS Code [here](https://marketplace.visualstudio.com/items?itemName=F5DevCentral.vscode-f5)

F5 AS3 Configuration Converter (ACC) Chariot [here](https://marketplace.visualstudio.com/items?itemName=F5DevCentral.vscode-f5-chariot)

## Journeys
Another tool for converting BIG IP configurations to AS3 [repo](https://github.com/f5devcentral/f5-journeys)

JOURNEYS is an application designed to assist F5 Customers with migrating a BIG-IP configuration to a new F5 device and enable new ways of migrating.

Supported journeys:

- Full Config migration - migrating a BIG-IP configuration from any version starting at 11.5.0 to a higher one, including VELOS and rSeries systems.
- Application Service migration - migrating mission critical Applications and their dependencies to a new AS3 configuration and deploying it to a BIG-IP instance of choice.

## Git

Some Git basics [here](https://snopsy.readthedocs.io/en/latest/module2/module2.html)

- What change
- Why the change was needed (commit message)
- Whom made the change (git push)
- Who approved the Change (merge request)

This also makes a perfect webhook for a CI tool like Jenkins

## Steps


*generic.json*

We will change the members section:

```
"members": [{
    "servicePort": 80,
    "serverAddresses": [
        "10.1.20.10"
    ]},
    {"servicePort": 81,
    "serverAddresses": [
        "10.1.20.11"
    ]
}]
```

Create Merge Request from forked repo, follow approval chain back to production branch.

# Deletions

You can delete all declarations

DELETE method to: `https://<mgmt-ip>/mgmt/shared/appsvcs/declare`

Delete declarations to a single Tenant

DELETE method to: `https://<mgmt-ip>/mgmt/shared/appsvcs/declare/<tenant>`

## iRule Tricks/Tips

Using this [link](https://clouddocs.f5.com/products/extensions/f5-appsvcs-extension/latest/refguide/declaration-purpose-function.html) serch for iRule Expansion Example, below that you will see AS3 POINTER SYNTAX, some cool tricks
